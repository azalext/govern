from azalinc.utils import *

from . import resources as resty

apiMETA.register(r'governements',  resty.GovernementViewSet)

apiDATA.register(r'govern/companies',  resty.CompanyViewSet)
apiDATA.register(r'govern/non-profit', resty.NonProfitViewSet)

from . import views

urlpatterns = [
    url(r'^$',                             views.homepage),

    #url(r'^(?P<uid>.+)/$',                 views.site_landing),
    #url(r'^(?P<uid>.+)/(?P<pid>[0-9]+)$',  views.site_product),
    #url(r'^(?P<uid>.+)/refresh/?$',        views.site_refresh),
    #url(r'^(?P<uid>.+)/dataset$',          views.site_dataset),
]

