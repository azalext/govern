# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from azalinc.shortcuts import *

from .tasks import *

################################################################################

def homepage(request):
    context = dict(
        listing=ShopifySite.objects.all(),
    )

    return render(request,'site_list.html', context)

################################################################################

def site_refresh(request, uid):
    shopping_download.delay(uid)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER','/#sync'))

################################################################################

from django.shortcuts import get_object_or_404

from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

class NonProfit_List(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'profile_list.html'

    def get(self, request):
        queryset = Profile.objects.all()
        return Response({'profiles': queryset})

class NonProfit_View(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'profile_detail.html'

    def get(self, request, pk):
        profile = get_object_or_404(Profile, pk=pk)
        serializer = ProfileSerializer(profile)
        return Response({'serializer': serializer, 'profile': profile})

    def post(self, request, pk):
        profile = get_object_or_404(Profile, pk=pk)
        serializer = ProfileSerializer(profile, data=request.data)
        if not serializer.is_valid():
            return Response({'serializer': serializer, 'profile': profile})
        serializer.save()
        return redirect('profile-list')

