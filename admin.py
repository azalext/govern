# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

####################################################################

class GovernementAdmin(admin.ModelAdmin):
    list_display  = ['where','alias','title']
    list_filter   = ['where']
    list_editable = ['alias','title']

admin.site.register(Governement, GovernementAdmin)

#*******************************************************************

class CompanyAdmin(admin.ModelAdmin):
    list_display  = ['where','alias','title']
    list_filter   = ['where']
    list_editable = ['alias','title']

admin.site.register(Company, CompanyAdmin)

####################################################################

class SocialAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    list_filter   = []
    list_editable = ['title']

admin.site.register(SocialActivity, SocialAdmin)

#*******************************************************************

class PopulationAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    list_filter   = []
    list_editable = ['title']

admin.site.register(Population, PopulationAdmin)

#*******************************************************************

class AssociativeAdmin(admin.ModelAdmin):
    list_display  = ['alias','title','phone','faxes','email','sites','pages','location','mission','populat']
    list_filter   = ['where','mission','populat']
    #list_editable = ['name','fqdn']

    def tools(self, record):
        resp = {}

        resp['View'] = "/govern/asso/%(id)s/"        % record.__dict__
        resp['Sync'] = "/govern/asso/%(id)s/refresh" % record.__dict__

        return '&nbsp;'.join(['<a href="%s">%s</a>' % (v,k) for k,v in resp.iteritems()])
    tools.allow_tags = True

    def count(self, record):
        return len(record.datasets.all())

admin.site.register(Associative, AssociativeAdmin)


























