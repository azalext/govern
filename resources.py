from .models import *

################################################################################

class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Company
        fields = ['where', 'alias', 'title']

#*******************************************************************************

class NonProfitSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = NonProfit
        fields = ['where', 'alias', 'title']

#*******************************************************************************

class GovernementSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Governement
        fields = ['where', 'alias', 'title']

################################################################################

class CompanyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Company.objects.all() #.order_by('-date_joined')
    serializer_class = CompanySerializer

#*******************************************************************************

class NonProfitViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = NonProfit.objects.all()
    serializer_class = NonProfitSerializer

#*******************************************************************************

class GovernementViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Governement.objects.all()
    serializer_class = GovernementSerializer

