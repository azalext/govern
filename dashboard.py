from controlcenter import Dashboard, widgets

from azalext.govern.models import *

################################################################################

class ActivityList(widgets.ItemList):
    model = SocialActivity
    list_display = ('alias','title')

class PopulationList(widgets.ItemList):
    model = Population
    list_display = ('alias','title')

################################################################################

class AssociativeChart(widgets.SingleBarChart):
    # label and series
    values_list = ('alias', 'score')
    # Data source
    queryset = Associative.objects.order_by('-score')
    limit_to = 3

################################################################################

class Landing(Dashboard):
    title = 'Govern'

    widgets = (
        AssociativeChart,

        ActivityList,
        PopulationList,
    )

