# -*- coding: utf-8 -*-

from azalinc.shortcuts import *

from azalinc.node.models import *

#*********************************************************************

BACKEND_TYPEs = (
    ('cache', "Memcache daemon"),
    ('redis', "Redis or Sentinel"),

    ('sqldb', "MySQL or PostgreSQL"),
    ('nosql', "MongoDB database"),
    ('neo4j', "Neo4j instance"),

    ('parse', "Parse Server"),
    ('graph', "GraphQL engine"),

    ('queue', "AMQP instance"),
    ('topic', "MQTT instance"),
)

################################################################################

class RegionalZone(models.Model):
    alias = models.CharField(max_length=128)
    title = models.CharField(max_length=512, blank=True)

    def __str__(self): return str(self.title or self.alias)

    class Meta:
        verbose_name = "Regional Zone"
        verbose_name_plural = "Regional Zones"

#*******************************************************************************

class SocialActivity(models.Model):
    alias = models.CharField(max_length=128)
    title = models.CharField(max_length=512, blank=True)

    def __str__(self): return str(self.title or self.alias)

    class Meta:
        verbose_name = "Social Activity"
        verbose_name_plural = "Social Activities"

#*******************************************************************************

class Population(models.Model):
    alias = models.CharField(max_length=128)
    title = models.CharField(max_length=512, blank=True)

    def __str__(self): return str(self.title or self.alias)

    class Meta:
        verbose_name = "Population"
        verbose_name_plural = "Populations"

#*******************************************************************************

class Associative(models.Model):
    where = models.CharField(max_length=128)
    alias = models.CharField(max_length=128)
    title = models.CharField(max_length=512, blank=True)

    phone = PhoneNumberField(blank=True)
    faxes = PhoneNumberField(blank=True)
    email = models.EmailField(blank=True)

    sites = models.URLField(blank=True)
    pages = models.CharField(max_length=512, blank=True)

    location = models.ForeignKey(RegionalZone, related_name='asso', blank=True, null=True)
    creation = models.DateTimeField(auto_now_add=True) # location

    populat = models.ForeignKey(Population, related_name='asso', blank=True, null=True)

    founded = models.TextField(blank=True)
    mission = models.TextField(blank=True)

    activ_p = models.ManyToManyField(SocialActivity, related_name='asso_p', blank=True)
    activ_s = models.ManyToManyField(SocialActivity, related_name='asso_s', blank=True)

    def __str__(self): return str(self.title or self.alias)

    class Meta:
        verbose_name = "Associative"
        verbose_name_plural = "Associatives"

################################################################################

class NonProfit(models.Model):
    where = models.CharField(max_length=128)
    alias = models.CharField(max_length=128)
    title = models.CharField(max_length=512, blank=True)

    phone = PhoneNumberField(blank=True)
    faxes = PhoneNumberField(blank=True)
    email = models.EmailField(blank=True)

    sites = models.URLField(blank=True)
    pages = models.CharField(max_length=512, blank=True)

    location = models.CharField(max_length=512, blank=True) # location
    creation = models.DateTimeField(auto_now_add=True) # location

    founded = models.TextField(blank=True)
    mission = models.TextField(blank=True)
    populat = models.CharField(max_length=512, blank=True)
    activ_p = models.TextField(blank=True)
    activ_s = models.TextField(blank=True)

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Non-Profit Organization"
        verbose_name_plural = "Non-Profit Organizations"

#*******************************************************************************

class Company(models.Model):
    where = models.CharField(max_length=128)
    alias = models.CharField(max_length=128)
    title = models.CharField(max_length=256)

    def __str__(self): return str(self.name)

    class Meta:
        verbose_name = "Business Company"
        verbose_name_plural = "Business Companies"

################################################################################

class Governement(models.Model):
    where = models.CharField(max_length=128)
    alias = models.CharField(max_length=128)
    title = models.CharField(max_length=256)

    def save(self, *args, **kwargs):
        resp = super(ShopifySite, self).save(*args, **kwargs)

        if self.sync:
            from .tasks import shopping_download

            shopping_download.delay(self.pk)

        return resp

    def __str__(self): return str(self.name)

    class Meta:
        verbose_name = "Governement"
        verbose_name_plural = "Governements"

#*******************************************************************************

class Secretary(models.Model):
    gover = models.ForeignKey(Governement, related_name="secretary")
    alias = models.CharField(max_length=128)
    title = models.CharField(max_length=256)

    def save(self, *args, **kwargs):
        resp = super(ShopifySite, self).save(*args, **kwargs)

        if self.sync:
            from .tasks import shopping_download

            shopping_download.delay(self.pk)

        return resp

    def __str__(self): return str(self.name)

    class Meta:
        verbose_name = "Secretary"
        verbose_name_plural = "Secretaries"

#*******************************************************************************

class Ministry(models.Model):
    gover = models.ForeignKey(Governement, related_name="minister")
    alias = models.CharField(max_length=128)
    title = models.CharField(max_length=256)

    def save(self, *args, **kwargs):
        resp = super(ShopifySite, self).save(*args, **kwargs)

        if self.sync:
            from .tasks import shopping_download

            shopping_download.delay(self.pk)

        return resp

    def __str__(self): return str(self.name)

    class Meta:
        verbose_name = "Ministry"
        verbose_name_plural = "Ministries"

